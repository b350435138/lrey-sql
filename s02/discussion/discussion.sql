Connection to MySQL via Terminal: mysql - u root
	-- -u : stands for username
	-- root : default username for sql
	-- -p : stands for password
	-- An empty values is the default password for SQL

-- Lists down the databases inside the DBMS
SHOW DATABASES;

	-- Note:
		-- SQL QUERIES is case insensitive, but to easily idenfitfy the queries we usually use UPPERCASE.
		-- ; : is a basic requirement of sql syntax


--==========================================================================


-- Create a Database
CREATE DATABASE music_db;

	-- Note: Naming convention in SQL Database and table uses the snake case.

-- Remove a specific database
DROP DATABASE music_db;

-- Select the database
USE music_db;

-- Create tables
	-- Table columns syntax: 
		-- [column_name] [data_type] [other_options]

-- Create user table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50),

	PRIMARY KEY (id)
);

-- Shows tables under a database
SHOW TABLES;

-- Describing a table allows us to see the table columns, data_types and extra_options
DESCRIBE users;

/*
    mini-activity:
        1. create a table for artists
        2. artists should have an id
        3. artists is required to have a name with 50 character limits
        4. assign the primary key to its id
        5. send a screenshot of your terminal in the batch space.
*/
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	userName VARCHAR(50) NOT NULL,

	PRIMARY KEY (id)
);


--======================================================================


-- Tables with foreign keys;
-- Applying constraints in a table
-- Syntax
--     CONSTRAINT foreign_key_name
--         FOREIGN KEY (column_name)
--         REFERENCES table_name(id)
--         ON UPDATE ACTION
--         ON DELETE ACTION


-- When to create a constraints (foreign key)?
	-- If the relationship is one-to-one, "primary key" of the parent row is added as "foreign key" of the child row.
	-- If the relationship is one-to-many, "foreign key" column is added in the "many" entity/table.
	-- If the relationship is "many-to-many", linking table is created to contain the "foreign key" for both tables/entities

-- Create albums table
CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id FOREIGN KEY (artist_id) REFERENCES artists(id)

	ON UPDATE CASCADE	
	ON DELETE RESTRICT
);



/*
  mini-activity
  -1. Create a table for songs
  -2. Put a required auto increment id
  -3. declare a song name with 50 char limit, this should be required
  -4. declare a length with the data type time and it should be required
  -5. declare a genre with 50 char limit, it should be required
  -6. declare an integer as album_id that should be required
  -7. create a primary key referring to the id of the songs
  8. create a foreign key constraint and name it fk_songs_album_id
  		8.a this should be referred to the album id
    	8.b it should have a cascaded update and restricted delete
  9. run create table songs in the terminal.
  10. Execute the SHOW tables to check if the table is created.
  11. Send your output in Hangouts
*/


CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	genre VARCHAR(50),
	album_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id FOREIGN KEY(album_id) REFERENCES albums(id)

	ON UPDATE CASCADE
	ON DELETE RESTRICT
);


--======================================================================

-- Create playlist table
CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	datetime_created DATETIME NOT NULL,
	user_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Create a joining playlists songs table
CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id FOREIGN KEY (playlist_id) REFERENCES playlists(id)

	ON UPDATE CASCADE
	ON DELETE RESTRICT,

	CONSTRAINT fk_playliss_songs_songs_id
	FOREIGN KEY (song_id) REFERENCES songs(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);




--======================================================================
--======================================================================
--======================================================================





CREATE DATABASE music_db;

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50),

	PRIMARY KEY (id)
);

CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	userName VARCHAR(50) NOT NULL,

	PRIMARY KEY (id)
);

CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id FOREIGN KEY (artist_id) REFERENCES artists(id)

	ON UPDATE CASCADE	
	ON DELETE RESTRICT
);

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	genre VARCHAR(50),
	album_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id FOREIGN KEY(album_id) REFERENCES albums(id)

	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	datetime_created DATETIME NOT NULL,
	user_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id FOREIGN KEY (user_id) REFERENCES users(id)
	
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,

	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id FOREIGN KEY (playlist_id) REFERENCES playlists(id)

	ON UPDATE CASCADE
	ON DELETE RESTRICT,

	CONSTRAINT fk_playlists_songs_songs_id FOREIGN KEY (song_id) REFERENCES songs(id)

	ON UPDATE CASCADE
	ON DELETE RESTRICT
);