-- MYSQL CRUD Operations

--[SECTION] Inserting a Record =====================================
	-- Syntax:
		-- INSERT INTO table_name(column_name) 
		-- VALUES (value1);
		
INSERT INTO artists (userName) VALUES ("Psy");
INSERT INTO artists (userName) VALUES ("Rivermaya");

-- Inserting a record with multiple columns.
	-- Syntax:
		-- INSERT INTO table_name(column_nameA, column_nameB) VALUES(valueA, ValueB);

-- Albums Table
INSERT INTO albums (album_title, date_released, artist_id) VALUES("Psy 6", "2013-08-15", 1);

INSERT INTO albums (album_title, date_released, artist_id) VALUES("Trip", "2013-08-15", 1);

-- Songs Table
-- MySQL non-delimeter Date and Time literals
INSERT INTO songs(song_name, length, genre, album_id) VALUES("Gangnam Style", "00:03:39", "K-pop", 1);
	-- This allows date and time values to be represented without delimeters (such as hyphen, colon or spaces)
	-- Time Formal:
		-- Original: HH:MM:SS
		-- Non-delimiter: HHMMSS
		-- But make sure that it still follows the correct "max" value for each time intervals:
			-- HH = 24
			-- MM = 59
			-- SS = 59

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kundiman", "00:05:24", "OPM", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kisapmata", "00:04:41", "OPM", 2);





-- [SECTION] Selecting Records =====================================

-- Showing all record details
-- SYNTAX:
	-- SELECT * FROM table_name;
	-- (*) means all columns will be shown in the selected table.

SELECT * FROM songs;

-- Show records with selected columns.
-- Syntax:
	-- Select Column_nameA, column_nameB FROM table_name;

-- Display the title and genre of all songs.
SELECT song_name, genre FROM songs;

-- Display album title and date released of all albums.
SELECT album_title, date_released FROM albums;

-- Show records that meets a certain condition.
-- SYNTAX:
	-- SELECT column_name FROM table_name WHERE condition;
	-- "WHERE" clause is used to filter records and to extract only those records that fulfull a specific condition.

-- Display the title of all the OPM songs;
SELECT song_name FROM songs WHERE genre = "OPM";

-- MINI ACTIVITY
-- Dsiplay the title of all songs where length is < 4mins.
SELECT song_name FROM songs WHERE length < 400;

-- Show records with multiple conditions
-- SYNTAX:
	-- AND clause
		-- SELECT column_name FROM table_name WHERE condition1 AND condition2

	-- OR clause
		-- SELECT column_name FROM table_name Where condition1 OR condition2

-- Display the title and length of OPM songs with more than 4mins and 30sec.
SELECT song_name, length FROM songs where length > 430 AND genre = "OPM";




-- [SECTION] Updating Records =====================================

-- Updating signle column of a record
-- Syntax: 
	-- UPDATE table_name SET column_name = new_value WHERE condition;

-- Update the length of kundiman to 4mins and 24secs.
UPDATE songs SET length = 424 WHERE song_name = "Kundiman";

-- Check if the song Kundiman is updated.
SELECT song_name, length FROM songs WHERE song_name = "Kundiman";

-- Updating multiple columns of records.
-- Syntax:
	-- UPDATE table_name SET column_name1 = new_value1, column_name2 = new_value2 WHERE condition;

-- Update the album with a title of Psy 6 and change its title to "Psy6 (Six Rules" and date released to July 15, 2012.
UPDATE albums SET album_title = "Psy 6 (Six Rules)", date_released = 20120715 WHERE album_title = "Psy 6";

-- Show the update for Psy 6
SELECT * FROM albums WHERE album_title = "Psy 6 (Six Rules)";

-- Deleting a record
-- SYNTAX:
	-- DELETE FROM table_name WHERE condition;
	-- Note: Removing the WHERE clause will remove all rows in the table.

-- DELETE all OPM songs that are more than 4 minutes and 30 seconds.
DELETE FROM songs WHERE genre = "OPM" AND length > 430;