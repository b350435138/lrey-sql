	--USE blog_db;

	--SHOW TABLES;

	-- DESCRIBE users;
	-- DESCRIBE posts;

-- 2. Add the ff records to the blog_db database: users and posts
	-- SELECT * FROM users;
	INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
	INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");
	
	-- SELECT * FROM posts;
	INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ("1", "First Code", "Hello World!", "2021-01-02 01:00:00");
	INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ("1", "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
	INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ("2", "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
	INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ("4", "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- 3. get all the post with an Author ID of 1
	SELECT * FROM posts WHERE author_id = "1";

-- 4. Get all the user's email and datetime of creation
	SELECT email, datetime_created FROM users;

-- 5. Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID.
	UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = "2";

-- 6. Delete the user with an email of "johndoe@gmail.com"
	DELETE FROM users WHERE email = "johndoe@gmail.com";



-- STRETCH GOALS
-- Users Table
	INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("johndoe", "john1234", "John Doe", 09123456789, "john@mail.com", "Quezon City");

	SELECT * FROM users;

-- Playlists Table
	INSERT INTO playlists (user_id, datetime_created) VALUES (1, 20230920080000);

	SELECT * FROM playlists;

-- Playlists songs Table
	INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1), (1, 2);

	SELECT * FROM playlists_songs;

